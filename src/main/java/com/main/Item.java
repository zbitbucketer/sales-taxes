package com.main;

public class Item {
	private final double BASIC_SALES_TAX = 0.1;
	private final double IMPORT_DUTY = 0.05;

	private String type;
	private double crudePrice;
	private double taxedPrice;
	private boolean imported;
	private int quantity;

	Item(String type, double crudePrice, boolean imported) {
		this.type = type;
		this.crudePrice = crudePrice;
		this.quantity = 1;
		this.imported = imported;
		this.taxedPrice = crudePrice;

		if (!type.equals("BOOK") && !type.equals("FOOD") && !type.equals("EXEMPT MEDICAL PRODUCT"))
			this.taxedPrice = crudePrice + (crudePrice * BASIC_SALES_TAX);
		if (imported)
			this.taxedPrice = this.taxedPrice + (this.taxedPrice * IMPORT_DUTY);
	}

	Item(String type, double crudePrice) {
		this(type, crudePrice, false);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getCrudePrice() {
		return crudePrice;
	}

	public void setCrudePrice(double crudePrice) {
		this.crudePrice = crudePrice;
	}

	public double getTaxedPrice() {
		return taxedPrice;
	}

	public void setTaxedPrice(double taxedPrice) {
		this.taxedPrice = taxedPrice;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
