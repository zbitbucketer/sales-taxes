package com.main;

public class Main {

	public static void main(String[] args) {
		// INPUT 1
		Item book = new Item("BOOK", 12.49);
		Item musicCD = new Item("MUSIC CD", 14.99);
		Item chocolateBar = new Item("CHOCOLATE BAR", 0.85);
		Basket basket = new Basket();
		basket.addItem(book);
		basket.addItem(musicCD);
		basket.addItem(chocolateBar);
		
		// OUTPUT1
		for (Item item : basket.getItems()) {
			System.out.println(item.getQuantity() + " " + item.getType() + " : " + item.getTaxedPrice());
		}
		System.out.println("Sales Taxes : " + basket.getSalesTaxes());
		System.out.println("Total : " + basket.getTotalTaxedPrice());

		// INPUT 2
		Item importedBoxOfChocolates = new Item("IMPORTED BOX OF CHOCOLATES", 10.00, true);
		Item importedBottleOfPerfume = new Item("IMPORTED BOTTLE OF PERFUME", 47.50, true);
		Basket basket2 = new Basket();
		basket2.addItem(importedBoxOfChocolates);
		basket2.addItem(importedBottleOfPerfume);

		// OUTPUT 2
		for (Item item : basket2.getItems()) {
			System.out.println(item.getQuantity() + " " + item.getType() + " : " + item.getTaxedPrice());
		}
		System.out.println("Sales Taxes : " + basket2.getSalesTaxes());
		System.out.println("Total : " + basket2.getTotalTaxedPrice());

		// INPUT 3
		Item importedBottleOfPerfume2 = new Item("IMPORTED BOTTLE OF PERFUME2", 27.99, true);
		Item bottleOfPerfume = new Item("BOTTLE OF PERFUME", 18.99);
		Item packetOfHeadachePills = new Item("PACKET OF HEADACHE PILLS", 9.75);
		Item importedBoxOfChocolates2 = new Item("IMPORTED BOX OF CHOCOLATES2", 11.25, true);
		Basket basket3 = new Basket();
		basket3.addItem(importedBottleOfPerfume2);
		basket3.addItem(bottleOfPerfume);
		basket3.addItem(packetOfHeadachePills);
		basket3.addItem(importedBoxOfChocolates2);

		// OUTPUT 3
		for (Item item : basket3.getItems()) {
			System.out.println(item.getQuantity() + " " + item.getType() + " : " + item.getTaxedPrice());
		}
		System.out.println("Sales Taxes : " + basket3.getSalesTaxes());
		System.out.println("Total : " + basket3.getTotalTaxedPrice());
	}
}
