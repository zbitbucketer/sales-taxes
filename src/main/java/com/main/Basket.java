package com.main;

import java.util.ArrayList;
import java.util.List;

public class Basket {
	private List<Item> items;
	private double totalCrudePrice;
	private double totalTaxedPrice;
	private double salesTaxes;

	Basket() {
	};

	Basket(List<Item> items) {
		this.items = items;
		for (Item item : items) {
			this.totalTaxedPrice += item.getTaxedPrice() * item.getQuantity();
			this.totalCrudePrice += item.getCrudePrice() * item.getQuantity();
		}
		this.salesTaxes = this.totalTaxedPrice - this.totalCrudePrice;
	}

	public void addItem(Item item) {
		if (this.items == null)
			this.items = new ArrayList<Item>();
		if (this.items.contains(item))
			item.setQuantity(item.getQuantity() + 1);
		else
			this.items.add(item);
		this.totalTaxedPrice += item.getTaxedPrice();
		this.totalCrudePrice += item.getCrudePrice();
		this.salesTaxes += item.getTaxedPrice() - item.getCrudePrice();
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public double getTotalCrudePrice() {
		return totalCrudePrice;
	}

	public void setTotalCrudePrice(double totalCrudePrice) {
		this.totalCrudePrice = totalCrudePrice;
	}

	public double getTotalTaxedPrice() {
		return totalTaxedPrice;
	}

	public void setTotalTaxedPrice(double totalTaxedPrice) {
		this.totalTaxedPrice = totalTaxedPrice;
	}

	public double getSalesTaxes() {
		return salesTaxes;
	}

	public void setSalesTaxes(double salesTaxes) {
		this.salesTaxes = salesTaxes;
	}

}
